import React, { Component } from 'react';
import './App.css';
import { Routes, Route, Link } from "react-router-dom";
import Signup from './components/Signup';
import Login from './components/Login'
import Groceries from './components/Groceries';
import Home from './components/Home';
import Basket from './components/Basket';
import CategoryGroceries from './components/categoryGroceries';

 class App extends Component {

  render(){
    return (
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/groceries" element={<Groceries />} />
          <Route path="/login" element={<Login />} />
          <Route path="/basket" element={<Basket />} />
          <Route path="/categoryGroceries" element={<CategoryGroceries />} />
        </Routes>
  
      </div>
    );
  }

}

export default App;
