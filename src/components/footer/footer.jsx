import React, { Component } from "react";
import playstore from "./images/bbgooglestore.png";
import appleStore from "./images/bbappstore.png";

import "./Footer.css";

export default class Footer extends Component {
  render() {
    return (
      <div className="container my-3 textSize">
        <div
          className="d-flex flex-column align-items-center"
          style={{ gap: "50px" }}
        >
          <div
            className="d-flex flex-row w-100 justify-content-between"
            style={{ gap: "50px" }}
          >
            <div
              className="d-flex flex-column"
              style={{ width: "150px", gap: "10px" }}
            >
              <span>Big Basket</span>
              <span> About Us</span>
              <span> In News</span>
              <span>Green bigbasket</span>
              <span>Privacy Policy </span>
              <span> Affiliate</span>
              <span> Terms and Conditions</span>
              <span> Careers At bigbasket</span>
              <span> bb Instant</span>
              <span>bb Daily</span>
              <span>bb Blog</span>
            </div>
            <div
              className="d-flex flex-column"
              style={{ width: "200px", gap: "10px" }}
            >
              <span>Help</span>
              <span>FAQs</span>
              <span>Contact Us</span>
              <span>bb Wallet FAQs</span>
              <span>bb Wallet T&amp;Cs</span>
              <span>Vendor Connect</span>
              <span>E-Invoice Compliance for bigbasket Vendors</span>
            </div>

 
          </div>

          <div
            className="d-flex flex-column align-items-center"
            style={{ gap: "10px" }}
          >

          </div>
        </div>
      </div>
    );
  }
}
