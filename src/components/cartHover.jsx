import React, { Component } from 'react';

class CartHover extends Component {
  render() {
    const {item} = this.props
    return <>
    <div className='hover-container'>
      <div>
        <img src={item.img} alt="product image" />
      </div>
      <div>
        <p>{item.name}</p>
        <p style={{fontSize:'16px'}}>{item.category}</p>
      </div>
      <div style={{fontWeight:'700', marginTop:'auto', marginBottom:'auto'}}>Qyt: 1</div>
      <div style={{fontWeight:'700', marginTop:'auto', marginBottom:'auto'}}>{item.price}</div>
    </div>
    </>

  }
}

export default CartHover;
