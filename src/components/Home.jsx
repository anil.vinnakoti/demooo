import React, { Component } from 'react';
import Body from './body/body';
import Footer from './footer/footer';
import Navbar from './navbar';
import Carousel from './body/carousel';

export class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Carousel />
        <Body />
        <Footer />
      </React.Fragment>
    )
  }
}

export default Home;
