import React, { Component } from 'react';
import { connect } from 'react-redux'
import { userlogIn } from '../redux'
import validator from 'validator';
import { Link } from 'react-router-dom';
import Navbar from './navbar';
import Footer from './footer/footer'






const mapStateToProps = state => {
  console.log(state);
  return {
    userData: state.userData,
    login: state.login,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userlogIn: (data) => dispatch(userlogIn(data)),
  }
}



class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Email: '',
      Password: '',
      isError: false,
      emailInValid: false,
      passInValid: false
    }
  }
  loginHandler = () => {
    const temp = true;
    this.props.userlogIn(temp)
  }


  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  validateForm = () => {
    const { Email, Password } = this.state;
    let isError = false;
    this.setState({
      isError: false,
      emailInValid: false,
      passInValid: false
    })

    if (validator.isEmpty(Email)) {
      console.log(isError, 'Email can not be empty')
      isError = true
      this.setState({
        isError: true,
        emailInValid: 'Email can not be empty'
      })
    }

    if (validator.isEmpty(Password)) {
      console.log(isError, 'Password field can not be empty')
      isError = true
      this.setState({
        isError: true,
        passInValid: 'Password field can not be empty'
      })
    }
    return isError
  }
  validateUserLogin = () => {
    const { Email, Password } = this.state;
    let userEmail = this.props.userData.Email;
    let userPassword = this.props.userData.Password;
    if (Email === userEmail && Password === userPassword) {
      this.props.userlogIn(true);
    } else {
      this.setState({
        isError: true,
        Email: '',
        Password: '',
        emailInValid: "Email id and password did not match",
        passInValid: "Email id and password did not match",
      })
    }
  }
  submitHandler = e => {
    e.preventDefault()
    let validateForm = this.validateForm();
    console.log('I am after validator', this.state.isError, validateForm)
    if (validateForm !== true) {
      this.validateUserLogin();
    }
  }
  render() {
    const { Email, Password, passInValid, emailInValid, isError } = this.state;
    return <div>
      {this.props.login ?

        <div>
          
        </div> :

        <div>
          <Navbar />
          <div className="d-flex align-items-center container-fluid vh-75 text-start">
            <div className="w-50 mx-auto my-5">
              <div style={{borderRadius:'20px'}} className="container w-50 p-5 border border-black bg-white login-card">
                <h3 style={{textALign: 'center'}} className='text-center' >Login</h3>
                <form onSubmit={this.submitHandler} autoComplete="new-password" >
                  <div className="mb-3">
                    <label className="form-label">Email address</label>
                    <input type="email" name="Email" value={Email} className="form-control" id="Email" aria-describedby="emailHelp" onChange={this.changeHandler} placeholder="Enter email" />
                    <p className="text-danger text-center mt-1">
                      {isError ? emailInValid : undefined}
                    </p>
                  </div>
                  <div className="mb-3">
                    <label className="form-label">Password</label>
                    <input type="password" name="Password" value={Password} className="form-control" id="Password" onChange={this.changeHandler} placeholder="Enter password" autoComplete="new-password" />
                    <p className="text-danger text-center mt-1">
                      {isError ? passInValid : undefined}
                    </p>
                  </div>
                  <Link to={{ pathname: "/" }}>
                    <button onClick={this.loginHandler} type="submit" style={{backgroundColor:'#9ACD32', border:'none'}} className="btn btn btn-success btn-primary-dark my-2 px-5 w-100">Login</button>
                  </Link>

                  <div className="mb-3 form-check text-center pt-3 ">
                    <a style={{textDecoration:'none', color: '#9ACD32'}} target="_blank" href="#" className="text-black ">Forgot password ?</a>
                  </div>
                </form>

              </div>
            </div>
          </div>
          <Footer />
        </div>
      }

    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
