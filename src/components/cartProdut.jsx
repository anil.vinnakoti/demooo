import React, { Component } from 'react';


class CartProduct extends Component {


  render() { 
    const {item} = this.props
    return <div className='product-card'>
      <div className='image-container'>
        <img  src={item.img} alt=" item image" />
      </div>

      <div className='labels-container'>
          <div className='right-names'>
            <p className='label-name' >{item.name}</p>
            <p className='label-category'>{item.category}</p>
            <p className='label-price'>Rs. {item.price}</p>
          </div>
          <div className='left-labels'>
            <div style={{marginBottom:'50px'}}>
              <img src='https://cdn1.iconfinder.com/data/icons/hand-signs-emotions-bright-flat-design/128/star-favorite-rating-important-512.png' />
              <p className='label-rating'>{item.ratings}</p>
            </div>
              <div onClick={this.props.removeHandler} className='delete-cartButton' style={{marginTop:'-60px'}}>
                <button data-id={item.id} style={{ color:'white'}} className='delete-cart '>-</button>
              </div>
              <div className='qty' style={{fontWeight:'700', color: '#9ACD32',marginBottom:'18px' }}> 
                Qty: 1
              </div>
          </div>
      </div>
    </div>;
  }
}
 
export default  CartProduct;