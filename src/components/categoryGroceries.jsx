import React, { Component } from 'react';
import {getGroceries, updateCart, userlogIn, userData, getCategory} from '../redux'
import { connect } from 'react-redux'
import Product from './Product';
import Navbar from './navbar';
import Footer from './footer/footer';


const mapStateToProps = state => {

  return {
    productDetails: state.groceries,
    cart: state.cart,
    login: state.login,
    user:state.user,
    category: state.category
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data)),
    userlogIn: (data) => dispatch(userlogIn(data)),
    userData : (data) => dispatch(userData(data)),
    getCategory: (data) => dispatch(getCategory(data))
  }
}

export class CategoryGroceries extends Component {
  render() {
    
    const categoryArray = this.props.productDetails.filter(item => item.category === this.props.category)

    return <>
        <Navbar  searchHandler={this.searchHandler} />

              <div className='groceries-container'>
                {categoryArray.map(item =>
                <div key={item.id}>
                    <Product addToCartHandler={this.addToCartHandler} item={item} />
                </div>)}
              </div>
        <Footer />

    </>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (CategoryGroceries);
