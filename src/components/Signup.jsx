import React, { Component } from 'react';
import { connect } from 'react-redux'
import { userData } from '../redux'
import validator from 'validator';
import { Link } from 'react-router-dom';
import Navbar from './navbar';
import Footer from './footer/footer'


const mapStateToProps = state => {
  console.log(state);
  return {
    userData: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userData: (data) => dispatch(userData(data)),
  }
}


class Signup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Name: '',
      Email: '',
      Password: '',
      Password2: '',
      isError: false,
      nameInValid: false,
      emailInValid: false,
      passInValid: false,
      checked:false
    }
  }
  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value },
      () => {
        const temp = this.state;
        this.props.userData(temp)
      });
  }
  validateForm = () => {
    const { Name, Email, Password, Password2, } = this.state;
    let isError = false;
    this.setState({
      isError: false,
      nameInValid: false,
      emailInValid: false,
      passInValid: false
    })
    if (validator.isEmpty(Name)) {
      isError = true
      this.setState({
        isError: true,
        nameInValid: 'Please Enter name'
      })
    } else if (validator.isAlpha(Name, 'en-US') === false) {
      isError = true
      this.setState({
        isError: true,
        nameInValid: 'Enter valid Name'
      })
    }

    if (validator.isEmpty(Email)) {
      isError = true
      this.setState({
        isError: true,
        emailInValid: 'Please enter Email'
      })
    } else if (validator.isEmail(Email, { blacklisted_chars: ' ' }) === false) {
      isError = true
      this.setState({
        isError: true,
        emailInValid: 'Enter valid Email'
      })
    }
    let checkLIst = {
      minLength: 8, minLowercase: 1,
      minUppercase: 1, minNumbers: 1
    }

    if (validator.isEmpty(Password)) {
      isError = true
      this.setState({
        isError: true,
        passInValid: 'Please enter Password'
      })
    } else if (validator.isStrongPassword(Password, checkLIst) === false) {
      isError = true
      this.setState({
        isError: true,
        passInValid: 'Password must contain atleast one UPPERCASE,lowercase, number, and special character'
      })
    }

    return isError
  }

  checkHnadler = () => {
    if(this.state.checked === false){
      this.setState({
        checked: true
      })
    }else{
      this.setState({
        checked: false
      })
    }


  }
  submitHandler = e => {
    e.preventDefault()
    let isError = this.validateForm();
    if (isError !== true) {
      let userdata = {
        ...this.state,
        isError: false,
        nameInValid: false,
        emailInValid: false,
        passInValid: false
      }
      this.props.addUserData(userdata)
      this.setState({
        Name: '',
        Email: '',
        Password: '',
        Password2: '',
        checked:false
      })

    }
  }


  render() {
    
    const { Name, Email, Password, Password2, passInValid, nameInValid, emailInValid, isError } = this.state;
    const formStyles ={
      borderRadius:'20px'
    }
    return <div>
      <Navbar/>
      <div className="d-flex align-items-center container-fluid vh-75 text-start">
        <div className="w-50 mx-auto my-5" >
          <div style={formStyles} className="container w-50 p-5 border border-black  login-card">
            <h3 className='text-center' style={{color:'#9ACD32', fontWeight:'700'}}>Sign Up Here</h3>
            <form onSubmit={this.submitHandler} autoComplete="new-password" >
              <div className="mb-3">
                <label className="form-label">Name</label>
                <input type="text" name="Name" value={Name} className="form-control" id="Name" aria-describedby="emailHelp" onChange={this.changeHandler} placeholder="Enter Name" />
                <p className="text-danger text-center mt-1">
                  {isError ? nameInValid : undefined}
                </p>
              </div>
              <div className="mb-3">
                <label className="form-label">Email address</label>
                <input type="email" name="Email" value={Email} className="form-control" id="Email"  onChange={this.changeHandler} placeholder="Enter email" />
                <p className="text-danger text-center mt-1">
                  {isError ? emailInValid : undefined}
                </p>
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input type="password" name="Password" value={Password} className="form-control" id="Password" onChange={this.changeHandler} placeholder="Enter password" autoComplete="new-password" />
                <p className="text-danger text-center mt-1">
                  {isError ? passInValid : undefined}
                </p>
              </div>
              <div className="mb-3">
                <label className="form-label"> Confirm Password</label>
                <input type="password" name="Password2" value={Password2} className="form-control" id="Password2" onChange={this.changeHandler} placeholder="Enter password" autoComplete="new-password" />
                <p className="text-danger text-center mt-1">
                  {isError ? passInValid : undefined}
                </p>
              </div>
              <label className="checks">
              <input
                type="checkbox"
                checked={this.state.checked}
                onChange={this.checkHnadler}
                name="confirmPassword"
                className="check"
                value={true}
              />
              <span className="terms" >
                I have read the terms &amp; conditions.
                <p className="errors text-danger">
                  
                </p>
              </span>
            </label>
            {/* <Link to={{ pathname: "/login" }}> */}
              <button style={{backgroundColor:'#9ACD32', border:'none'}} type="submit" className="btn btn btn-success btn-primary-dark my-2 px-5 w-100">Sign Me Up</button>
            {/* </Link> */}
            </form>
          </div>
        </div>
      </div>
      <Footer/>
    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
