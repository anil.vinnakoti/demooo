import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {getGroceries, updateCart, userlogIn, userData} from '../redux'
import { connect } from 'react-redux'
import CartHover from './cartHover';
import _ from 'lodash';

const mapStateToProps = state => {
  
  return {
    productDetails: state.groceries,
    cart: state.cart,
    login: state.login,
    user:state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data)),
    userlogIn: (data) => dispatch(userlogIn(data)),
    userData : (data) => dispatch(userData(data))
  }
}

class MyBasket extends Component {
  render() {
    const {cart} = this.props
    const cartData = _.flatten(cart)
    let total =0;
    for(let i=0;i<cartData.length;i++){
      total += parseFloat(cartData[i].price)
    }

    console.log(total);
    

    return <div className='basket'>
            <ul>
              <li className=''>
                <div className='d-flex'>
                    <img alt='basket iimage' src='https://www.rawshorts.com/freeicons/wp-content/uploads/2017/01/red_shoppictbasket_1484336512-1.png'/>

                  <div className='d-flex basket-labels'>
                    <p >My Basket</p>
                    <Link style={{textDecoration: 'none', color: 'black'}} to={{ pathname: "/basket" }}>
                      <p>{cart.length} ITEMS</p>
                    </Link>
                  </div>
                </div>


                <ul className='dropdown hover-main'>
                  {_.flatten(this.props.cart).map((item, index) => 
                    <li  key={index}><CartHover item={item} /></li>)}
                  <li className='hover-labels'>
                    <div className='d-flex'>
                      <p>Total:</p>
                      <p>Rs.{total}</p>
                    </div>
                    <Link to={{ pathname: "/basket" }}>
                      <button className='view-basket'>View Basket</button>
                    </Link>
                  </li>
                </ul>
              </li>


            </ul>

          </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBasket);
