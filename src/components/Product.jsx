import React, { Component } from 'react';
import { groceries} from "./data";
import { connect } from 'react-redux'
import {getGroceries, updateCart} from '../redux'


const mapStateToProps = state => {
  return {
    productDetails: state.groceries,
    cart: state.cart
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data))
  }
}


export class Product extends Component {
  addToCartHandler = (event) => {
    const productItem = groceries.filter(item => item.id == event.target.dataset.id)
    const temp = [...this.props.cart, productItem];
    this.props.updateCart(temp)
  }

  render() {
    const {item} = this.props
    return <div className='product-card'>
      <div className='image-container'>
        <img  src={item.img} alt=" item image" />
      </div>

      <div className='labels-container'>
          <div className='right-names'>
            <p className='label-name' >{item.name}</p>
            <p className='label-category'>{item.category}</p>
            <p className='label-price'>Rs. {item.price}</p>
          </div>
          <div className='left-labels'>
            <div >
              <img src='https://cdn1.iconfinder.com/data/icons/hand-signs-emotions-bright-flat-design/128/star-favorite-rating-important-512.png' />
              <p className='label-rating'>{item.ratings}</p>
            </div>
              <div onClick={this.addToCartHandler} className='add-cartButton'>
                <button data-id={item.id} style={{ color:'white'}} className='add-cart'>+</button>
              </div>
          </div>
      </div>
    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Product);
