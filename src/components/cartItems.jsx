import _ from 'lodash';
import React, { Component } from 'react';
import CartProduct from './cartProdut';
import Footer from './footer/footer';
import {getGroceries, updateCart, userlogIn, userData} from '../redux'
import { connect } from 'react-redux'

const mapStateToProps = state => {
  console.log(state);
  return {
    productDetails: state.groceries,
    cart: state.cart,
    login: state.login,
    user:state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data)),
    userlogIn: (data) => dispatch(userlogIn(data)),
    userData : (data) => dispatch(userData(data))
  }
}

class CartItems extends Component {

  removeHandler = (event) => {
    const newCart = _.flatten(this.props.cart)
    const temp = newCart.filter(item => item.id != event.target.dataset.id);
    this.props.updateCart(temp)
  }


  render() { 
   const {cartItems} = this.props
   let cartData = _.flatten(cartItems)

   let total = 0

   for(let i = 0; i< cartData.length;i++){
     total += parseFloat(cartData[i].price)
   }


    return (
      <>
      <div className='cart-container'>
        <div>
            {cartData.map((item, index) =>
            <div  key={index}>
                <CartProduct removeHandler={this.removeHandler} item ={item} />
            </div>)}
        </div>
      
      <div className='price-labels' >
        <div>
          <p style={{fontWeight:'700', textAlign:'center'}}>MY CART</p>
        </div>
        <div style={{display:'flex',marginTop: '50px', justifyContent:'space-between'}}>
          <div >
            <p style={{fontWeight:'700'}}> Number of Products:</p>
            <p style={{fontWeight:'700'}}> Total Amount: </p>

          </div>
          <div>
            <p style={{fontWeight:'700', color:'#9ACD32'}}> {cartData.length}</p>
            <p style={{fontWeight:'700', color:'#9ACD32'}}>Rs.{total}</p>
          </div>
        </div>

        <div className='check-out'>
          <button>Check Out</button>
        </div>


      </div>
    </div>
    <Footer />
      </>
      
    );
  }
}
 
export default connect(mapStateToProps, mapDispatchToProps) (CartItems);