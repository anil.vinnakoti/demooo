import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {getGroceries, updateCart, userlogIn, userData, getCategory} from '../redux'
import { connect } from 'react-redux'

const mapStateToProps = state => {

  return {
    productDetails: state.groceries,
    cart: state.cart,
    login: state.login,
    user:state.user,
    category: state.category
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data)),
    userlogIn: (data) => dispatch(userlogIn(data)),
    userData : (data) => dispatch(userData(data)),
    getCategory: (data) => dispatch(getCategory(data))
  }
}

export class ShopByCategory extends Component {

  categoryHandler = (event) => {
    const temp = event.target.textContent.toLowerCase();
    this.props.getCategory(temp)
  }


  render() {

    return <div className='shop-category'>
            <ul className='drop-down' >
                <li>
                    <p className='shop-title'>SHOP BY CATEGORY</p>
                    <ul className="dropdown">
                      <Link style={{textDecoration:'none'}} to={{ pathname: "/groceries" }}>
                        <li ><p  >All Products</p></li>
                      </Link>
                      <Link style={{textDecoration:'none'}} to={{pathname:'/categoryGroceries'}} >
                        <li onClick={this.categoryHandler} ><p  >Fruits</p></li>
                      </Link>
                      <Link style={{textDecoration:'none'}} to={{pathname:'/categoryGroceries'}} >
                        <li onClick={this.categoryHandler} ><p  >Vegetables</p></li>
                      </Link>
                      <Link style={{textDecoration:'none'}} to={{pathname:'/categoryGroceries'}} >
                        <li onClick={this.categoryHandler} ><p  >Beverages</p></li>
                      </Link>
                      <Link style={{textDecoration:'none'}} to={{pathname:'/categoryGroceries'}} >
                        <li onClick={this.categoryHandler} ><p  >Snacks</p></li>
                      </Link>
                      <Link style={{textDecoration:'none'}} to={{pathname:'/categoryGroceries'}} >
                        <li onClick={this.categoryHandler} ><p  >Cooking Essential</p></li>
                      </Link>
                    </ul>
                </li>
            </ul>
          </div>
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (ShopByCategory);
