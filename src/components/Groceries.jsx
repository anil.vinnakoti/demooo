import React, { Component } from 'react';
import Product from './Product';
import { connect } from 'react-redux'
import {getGroceries} from '../redux'
import Navbar from './navbar';
import {groceries} from '../components/data' 
import Footer from './footer/footer';

const mapStateToProps = state => {
  return {
    productDetails: state.groceries,
    cart: state.cart
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
  }
}


export class Groceries extends Component {

  state = {
    groceriesData : groceries,
    cartItems:this.props.cart.length
  }

  searchHandler = (event) => {
      const searchItems = this.state.groceriesData.filter((item) => item.name.toLowerCase().includes(event.target.value.trim()))
      if (searchItems.length !== 0) {
        this.setState({
          groceriesData: searchItems,
        });
      }else if(event.target.value === ''){
        this.setState({
          groceriesData: groceries
        })
      }
       else {
        this.setState({
          groceriesData: groceries
        });
      }
  

  }

  addToCartHandler = () => {
    this.setState({
      cartItems: this.state.cartItems +1
    })
  }

  render() {
    const {groceriesData} = this.state
    
    return (
      <>
        <div>
          <Navbar cartItems = {this.state.cartItems} searchHandler={this.searchHandler} />
          <div className='groceries-container'>
            {groceriesData.map(item =>
            <div key={item.id}>
                <Product addToCartHandler={this.addToCartHandler} item={item} />
            </div>)}
          </div>
        </div>
        <Footer />
      </>

      
  );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Groceries);
