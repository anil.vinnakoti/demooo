import React, { Component } from "react";

import first from "./big-basket-images/carasol/four.jpg";
import second from "./big-basket-images/carasol/two.jpg";
import third from "./big-basket-images/carasol/three.jpg";
import forth from "./big-basket-images/carasol/one.jpg";
import fifth from "./big-basket-images/carasol/five.jpg";
import sixth from "./big-basket-images/carasol/six.jpg";
import seventh from "./big-basket-images/carasol/seven.jpg";

class Carousel extends Component {
  render() {
    return (
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ol className="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            className="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src={first} alt="First slide" />
          </div>

        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>

        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true"></span>

        </a>
      </div>
    );
  }
}

export default Carousel