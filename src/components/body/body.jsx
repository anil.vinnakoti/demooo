import React, { Component } from "react";
import tea1 from "./big-basket-images/tea.jpg";
import deo from "./big-basket-images/deo.jpg";
import beard from "./big-basket-images/beard.jpg";
import coffee from "./big-basket-images/coffee.jpg";
import './Body.css'



export default class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
    };
  }

  readMore = () => {
    this.state.clicked = !this.state.clicked;
    this.setState({
      clicked: this.state.clicked,
    });
  };
  render() {
    return (
      <div className="container">
        <div>
          <div className="cat-banners ">
            <div className="border py-2 child-1">EGGS, MEAT AND FISH</div>
            <div className="border p-2 child-2 text-white">PRECAUTIONARY MEASURES</div>
            <div className="border p-2  child-2 text-white">
              FREQUENTLY ASKED QUESTIONS
            </div>
            <div className="border py-2 child-1">COMBO STORES</div>
            <div className="border py-2 child-1">DEALS OF THE WEEK</div>
          </div>

          <div>
            <div>
              <div
                className="text-center mb-4 mt-4 "
                style={{ fontSize: "24px", fontWeight:'700' }}
              >
                Bank Offers
              </div>
            </div>

            <div className="d-flex justify-content-center w-100">
              <div className="w-50 px-2">
                <img className="w-100" src='https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/dd2837f3-086b-4be1-a5b7-abc1f7524de9/Affiliates_Feb_CitiBank_360-310122.jpg' />
              </div>
              <div className="w-50 px-2">
                <img className="w-100" src='https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/dd2837f3-086b-4be1-a5b7-abc1f7524de9/Affiliates_Feb_Paytm_360-310122.jpg' />
              </div>
              <div className="w-50 px-2">
                <img className="w-100" src='https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/dd2837f3-086b-4be1-a5b7-abc1f7524de9/Affiliates_Feb_RBL_360-310122.jpg' />
              </div>
              <div className="w-50 px-2">
                <img className="w-100" src='https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/dd2837f3-086b-4be1-a5b7-abc1f7524de9/hp_aff_m_dbs-400_360_310122.jpg' />
              </div>
            </div>
          </div>
        </div>



        <div>
          <div className="text-center mb-4 mt-4 " style={{ fontSize: "24px",fontWeight:'700' }}>
            Most Popular
          </div>

          <div className="most-popular ">
            <div>
              <img src={tea1} />
            </div>
            <div>
              <img src={deo} />
            </div>
            <div>
              <img src={beard} />
            </div>
            <div>
              <img src={coffee} />
            </div>
          </div>

        </div>

        <div className="products">
        </div>

        <div>
          <div className="fruit-content text-center mb-4 mt-4 " style={{ fontSize: "24px", fontWeight:'700' }}>
            Fruits and Vegetables
          </div>
          {/* <div className="d-flex fruits-vegetables">
            <div className="under w-100 mt-4">
              <img
                src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/9535add6-5887-413f-9245-06181614e1c1/hp_fnvStorefront_under-rs-10-m_250122_01.jpg"
                className=""
              />
            </div>
            <div>
              <div className="d-flex">
                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/9535add6-5887-413f-9245-06181614e1c1/hp_fnvStorefront_fresh-fruits-m_251021_02.jpg"
                    className="deals-container"
                  />
                </div>

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/9535add6-5887-413f-9245-06181614e1c1/hp_fnvStorefront_cuts-sprputs-m_250122_04.jpg"
                    className="deals-container"
                  />
                </div>
              </div>
              <div className="d-flex flex-wrap">
                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/9535add6-5887-413f-9245-06181614e1c1/hp_fnvStorefront_fresh-veggs-m_251021_03.jpg"
                    className="deals-container"
                  />
                </div>
                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/9535add6-5887-413f-9245-06181614e1c1/hp_fnvStorefront_extic-corner-m_250122_05.jpg"
                    className="deals-container"
                  />
                </div>
              </div>
            </div>
          </div> */}

          <div>
            
            <div className="text-center mb-4 mt-4 " style={{ fontSize: "24px", fontWeight:'700' }}>
              Your Daily Staples
              <div className=" staples-container d-flex deals">

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/1d7a0d5e-b571-4d6a-953c-f3c69d794b75/hp_staplesStorefront_atta-m_480_250122_01.jpg"
                    className="deals-container"
                  />
                </div>

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/1d7a0d5e-b571-4d6a-953c-f3c69d794b75/hp_staplesStorefront_rice-m_480_250122_02.jpg"
                    className="deals-container"
                  />
                </div>

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/1d7a0d5e-b571-4d6a-953c-f3c69d794b75/hp_staplesStorefront_dals-m_480_250122_03.jpg"
                    className="deals-container"
                  />
                </div>

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/1d7a0d5e-b571-4d6a-953c-f3c69d794b75/hp_staplesStorefront_cooking-m_480_250122_04.jpg"
                    className="deals-container"
                  />
                </div>

                <div>
                  <img
                    src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/1d7a0d5e-b571-4d6a-953c-f3c69d794b75/hp_staplesStorefront_dry-m_480_250122_05.jpg"
                    className="deals-container"
                  />
                </div>

              </div>

            </div>

          </div>

          <div>
            <div className="text-center mb-4 mt-4 " style={{ fontSize: "24px", fontWeight:'700' }}>
              Snacks Store
            </div>
            <div className="snack-container d-flex deals">

              <div>
                <img
                  src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/f3e437d0-a6ca-41a9-b0a1-f441c57fe9ba/hp_snacksStorefront_namkeens-m_480_250122_01.jpg"
                  className="deals-container"
                />
              </div>

              <div>
                <img
                  src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/f3e437d0-a6ca-41a9-b0a1-f441c57fe9ba/hp_snacksStorefront_frozen-m_480_250122_02.jpg"
                  className="deals-container"
                />
              </div>

              <div>
                <img
                  src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/f3e437d0-a6ca-41a9-b0a1-f441c57fe9ba/hp_snacksStorefront_soups-noodles-m_480_250122_03.jpg"
                  className="deals-container"
                />
              </div>

              <div>
                <img
                  src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/f3e437d0-a6ca-41a9-b0a1-f441c57fe9ba/hp_snacksStorefront_biscuits-m_480_250122_04.jpg"
                  className="deals-container"
                />
              </div>

              <div>
                <img
                  src="https://www.bigbasket.com/media/customPage/b01eee88-e6bc-410e-993c-dedd012cf04b/bdcb2f5b-a1c4-4cd7-aab4-dd41e7a17532/f3e437d0-a6ca-41a9-b0a1-f441c57fe9ba/hp_snacksStorefront_namkeens-m_480_250122_01.jpg"
                  className="deals-container"
                />
              </div>
              
            </div>
          </div>
        </div>
        
        <div id= 'about'className=" mt-3 mb-5 ">
          <div>
            <h3>bigbasket - online grocery store</h3>
          </div>
          <div className="about-content">
            Did you ever imagine that the freshest of fruits and vegetables, top
            quality pulses and food grains, dairy products and hundreds of
            branded items could be handpicked and delivered to your home, all at
            the click of a button? India’s first comprehensive online megastore,
            bigbasket.com, brings a whopping 20000+ products with more than 1000
            brands, to over 4 million happy customers. From household cleaning
            products to beauty and makeup, bigbasket has everything you need for
            your daily needs. bigbasket.com is convenience personified We’ve
            taken away all the stress associated with shopping for daily
            essentials, and you can now order all your household products and
            even buy groceries online without travelling long distances or
            standing in serpentine queues. Add to this the convenience of
            finding all your requirements at one single source, along with great
            savings, and you will realize that bigbasket- India’s largest online
            supermarket, has revolutionized the way India shops for groceries.
            Online grocery shopping has never been easier. Need things fresh?
            Whether it’s fruits and vegetables or dairy and meat, we have this
            covered as well! Get fresh eggs, meat, fish and more online at your
            convenience. Hassle-free Home Delivery options
          </div>
          <div className="about-content">
            We deliver to 25 cities across India and maintain excellent delivery
            times, ensuring that all your products from groceries to snacks
            branded foods reach you in time.
          </div>
          <div>
            <ul>
              <li className="about-content">
                Slotted Delivery: Pick the most convenient delivery slot to have
                your grocery delivered. From early morning delivery for early
                birds, to late-night delivery for people who work the late
                shift, bigbasket caters to every schedule.
              </li>
              <li className="about-content">
                Express Delivery: This super useful service can be availed by
                customers in cities like Bangalore, Mumbai, Pune, Chennai,
                Kolkata, Hyderabad and Delhi-NCR in which we deliver your orders
                to your doorstep in 90 Minutes.
              </li>
              <li className="about-content">
                BB Specialty stores: Missed out on buying that essential item
                from your favorite neighborhood store for tonight’s party? We’ll
                deliver it for you! From bakery, sweets and meat to flowers and
                chocolates, we deliver your order in 90 minutes, through a
                special arrangement with a nearby specialty store, verified by
                us.
              </li>
              <button onClick={this.readMore} className="read-more btn">
                Read More..
              </button>
            </ul>
              <div id='about'>
                {this.state.clicked ? (
                  <div className="about-content">
                    <h3>India’s biggest Online Supermarket</h3>
                    bigbasket.com believes in providing the highest level of
                    customer service and is continuously innovating to meet
                    customer expectations. Our On-time Guarantee is one such
                    assurance where we refund 5% of the bill value if the delivery
                    is delayed (however, due to the pandemic caused by Covid-19
                    our delivery might get delayed. Delivery Guarantee will not be
                    applicable). For all your order values above Rs. 1200, we
                    provide free delivery. A wide range of imported and gourmet
                    products are available through our express delivery and
                    slotted delivery service. If you ever find an item missing on
                    delivery or want to return a product, you can report it to us
                    within 48 hours for a ‘no-questions-asked’ refund. Best
                    quality products for our quality-conscious customers.
                    bigbasket.com is synonymous with superior quality and
                    continues to strive for higher levels of customer trust and
                    confidence, by taking feedback and giving our customers what
                    they want. We have added the convenience of pre-cut fruits in
                    our Fresho range. If it’s a product category you’re looking to
                    shop from, we’ve made it convenient for you to access all
                    products in a section easily. For instance, if you’re looking
                    for beverages, you can order from a long list of beverages
                    that include cool drinks, hot teas, fruit juices and more. We
                    are proud to be associated closely with the farmers from whom
                    we source our fresh products. Most of our farm-fresh products
                    are sourced directly from farmers, which not only ensures the
                    best prices and freshest products for our customers but also
                    helps the farmers get better prices. With more than 80 Organic
                    Fruits and Vegetables and a wide range of organic staples,
                    bigbasket has the largest range in the organic products
                    category. When it comes to payment, we have made it easy for
                    our customers can pay through multiple payment channels like
                    Credit and Debit cards, Internet Banking, e-wallets and Sodexo
                    passes or simply pay Cash on Delivery (COD).The convenience of
                    shopping for home and daily needs, while not compromising on
                    quality, has made bigbasket.com the online supermarket of
                    choice for thousands of happy customers across India.
                  </div>
                ) : undefined}
              </div>
            </div>
        </div>
      </div>
    );
  }
}
