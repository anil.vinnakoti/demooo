import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ShopByCategory from './shopByCategory';
import {getGroceries, updateCart, userlogIn, userData} from '../redux'
import { connect } from 'react-redux'
import MyBasket from './MyBasket';


const mapStateToProps = state => {
  return {
    productDetails: state.groceries,
    cart: state.cart,
    login: state.login,
    user:state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data)),
    userlogIn: (data) => dispatch(userlogIn(data)),
    userData : (data) => dispatch(userData(data))
  }
}

class Navbar extends Component {

  logoutHandler =() => {
    const temp = false;
    this.props.userlogIn(temp)
  }


    render() { 
        return this.props.login 
        ? (
          <div className='main'>
              <div className='header'>

                <Link to={{ pathname: "/" }}>
                  <div className='logo-container'>
                    <img className='main-logo' alt='logo ' src='https://yesscale.com/wp-content/uploads/2019/05/bigbasket-logo.jpg' />
                  </div>
                </Link>

                <div className='search-container'>
                    <input onChange={this.props.searchHandler} type='text' placeholder='Serach here' />
                </div>

                <div className='user' >
                  <div style={{display:'flex', marginRight:'20px'}}>
                    <img style={{width:'50px'}} alt='user icon' src='http://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg' />
                    <h5 style={{marginTop:'10px'}}>{this.props.user.Name}</h5>
                  </div>


                    <button onClick={this.logoutHandler}>Log Out</button>

                </div>

                <div className='basket'>
                    <img alt='basket iimage' src='https://www.rawshorts.com/freeicons/wp-content/uploads/2017/01/red_shoppictbasket_1484336512-1.png'/>
                    <div className='basket-labels'>
                        <p>MY BASKET</p>
                        <Link style={{textDecoration: 'none', color: 'black'}} to={{ pathname: "/basket" }}>
                        <p >{this.props.cart.length} ITEMS</p>
                        </Link>
                    </div>
              
                </div>
              </div>

              <div className='second'>
                <ShopByCategory data={this.state} />
                <div style={{fontWeight:'700'}}>
                  OFFERS
                </div>
              </div>
              <div className='contact-location'>
                <p>BB SPECIALITY</p>
                <p>1860 123 1000</p>
                <p>560004, Banglore</p>
              </div>

          </div>
        
        ) :(
            <div className='main'>
                <div className='header'>

                  <Link to={{ pathname: "/" }}>
                    <div className='logo-container'>
                      <img alt='logo' className='main-logo' src='https://yesscale.com/wp-content/uploads/2019/05/bigbasket-logo.jpg' />
                    </div>
                  </Link>

                  <div className='search-container'>
                      <input onChange={this.props.searchHandler} type='text' placeholder='Serach here' />
                  </div>

                  <div className='user'>
                    <Link to={{ pathname: "/signup" }}>
                      <button>Sign Up</button>
                    </Link>

                    <Link to={{ pathname: "/login" }}>
                      <button>Log In</button>
                    </Link>

                  </div>

                  <MyBasket cart={this.props.cart} />

                </div>

                <div className='second'>
                  <ShopByCategory data={this.state} />
                  <div style={{fontWeight:'700'}}>
                    OFFERS
                  </div>
                </div>
                <div className='contact-location'>
                  <p>BB SPECIALITY</p>
                  <p>1860 123 1000</p>
                  <p>560004, Banglore</p>
                </div>

            </div>
          
          )
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps) (Navbar);