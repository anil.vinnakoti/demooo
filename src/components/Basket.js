import React, { Component } from "react";
import { getGroceries, updateCart } from "../redux";
import { connect } from "react-redux";
import CartItems from './cartItems'
import Navbar from "./navbar";
import Footer from "./footer/footer";

const mapStateToProps = state => {
  return {
    productDetails: state.groceries,
    cart: state.cart
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroceries: (data) => dispatch(getGroceries(data)),
    updateCart: (data) => dispatch(updateCart(data))

  }
}

class Basket extends Component {



  render() {
    const {cart} = this.props
    return cart.length === 0 
          ? (
            <div>
            <Navbar />
            <h3 className="text-center" style={{marginBottom: '25%'}}> No Item In Cart</h3>
            <Footer />
            </div>

          )
          :(
            <div>
              <Navbar />
              <CartItems cartItems={cart} />
            </div>
          );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Basket);
