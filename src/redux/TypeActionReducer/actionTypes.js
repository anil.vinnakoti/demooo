export const USER_DATA = "USER_DATA"
export const USER_LOGIN = "USER_LOGIN"
export const GROCERIES = 'GROCERIES'
export const CART = 'CART'
export const GET_CATEGORY = 'GET_CATEGORY'