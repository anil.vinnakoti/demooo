
import{groceries} from '../../components/data'
import { USER_DATA, USER_LOGIN,GROCERIES, CART, GET_CATEGORY } from './actionTypes';

const groceriesData = groceries

const initialState = {
  user: {},
  login: false,
  groceries: groceriesData,
  cart : [],
  category:''
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        user: action.payload
      };
    case USER_LOGIN:
      return {
        ...state,
        login: action.payload
      };
      case GROCERIES: return{
        ...state,
        groceries:action.payload
      }
      case CART: console.log(action.payload); return{
        
        ...state,
        cart:action.payload
      }
      case GET_CATEGORY: return{
        ...state,
        category: action.payload
      }
    default:
      return state
  }
}

export default userReducer