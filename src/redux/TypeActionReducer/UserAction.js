import { USER_DATA, USER_LOGIN, GROCERIES, CART, GET_CATEGORY } from './actionTypes'

export const userData = (details) => {
  return {
    type: USER_DATA,
    payload: details
  }
}

export const userlogIn = (loginState) => {
  return {
    type: USER_LOGIN,
    payload: loginState
  }
}

export const getGroceries = (data) => {
  return {
    type: GROCERIES,
    payload: data
  }
}

export const updateCart = (data) => {
  return {
    type: CART,
    payload: data
  }
}

export const getCategory = (data) => {
  return {
    type: GET_CATEGORY,
    payload: data
  }
}